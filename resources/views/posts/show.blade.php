@extends('adminlte.master')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2> Show Film</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('posts.index') }}"> Back</a>
            </div>
        </div>
    </div>
 
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Judul:</strong>
                <td>{{ $post->judul }}</td><br>
                <strong>Ringkasan:</strong>
            <td>{{ $post->ringkasan }}</td><br>
            <strong>Tahun:</strong>
            <td>{{ $post->tahun }}</td><br>
            <strong>Poster:</strong>
            <td>{{ $post->poster }}</td><br>
            </div>
        </div>
    </div>
@endsection